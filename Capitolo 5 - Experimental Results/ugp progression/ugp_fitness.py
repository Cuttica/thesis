toFilter = False

if toFilter:
	with open('ugp_progress.txt', 'r') as f:
		lines = f.readlines()
		filtered = ""#""x , y\n"
		counter = 0
		for index, line in enumerate(lines):
			if index % 5 == 0:
				counter += 1
				#filtered += "" + str(counter) + " , " + line
				filtered += line

		with open('ugp_processed.txt', 'w') as f2:
			f2.write(filtered)
else:
	with open('ugp_processed.txt', 'r') as f:
		intList = map(int, f.readlines())
		maxValue = max(intList)
		minValue = min(intList)
		print str(maxValue) + " " + str(minValue)
		toPrint = "x , y\n"
		for index, val in enumerate(intList):
			#toPrint += "" + str(index) + " , " + str((((val - minValue) / (maxValue - minValue)) - 1) * -1000) + "\n"
			toPrint += "" + str(index) + " , " + str((maxValue - val) * 1000 / (maxValue - minValue)) + "\n"

		with open('ugp_processed_relative.txt', 'w') as f2:
			f2.write(toPrint)